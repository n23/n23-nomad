# n23-nomad - GPS tracker based on N23 framework
#
# Copyright (C) 2019-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncio
import json
import logging
from dateutil.parser import parse as time_parse

logger = logging.getLogger(__name__)

"""
Functions to read and parse gpsd daemon data.
"""

REQUIRED = 'time', 'device', 'lon', 'lat', 'alt'

async def read_gps(port: int=2947):
    """
    Read and yield data from gpsd daemon.
    """
    reader, writer = await asyncio.open_connection(port=port)
    writer.write(b'?WATCH={"enable":true,"json":true}\n')
    logger.info('gps connection started')

    try:
        while True:
            line = await reader.readline()
            data = json.loads(line.decode())

            if __debug__:
                logger.debug('gps data: {}'.format(data))

            if data.get('mode') == 3 and has_required(data):
                data['time'] = time_parse(data['time'])
                yield data

    finally:
        writer.write(b'?WATCH={"enable":false}\n')
        logger.info('gps closed')

def has_required(data: dict) -> bool:
    """
    Check if GPS record has all required attributes.
    """
    return all(v in data for v in REQUIRED)

# vim: sw=4:et:ai
