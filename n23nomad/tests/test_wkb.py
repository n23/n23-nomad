# n23-nomad - GPS tracker based on N23 framework
#
# Copyright (C) 2019-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for WKB conversion functions.
"""

from binascii import hexlify

from ..wkb import to_ewkb

import pytest

test_positions = (
    (-6.271687504, 53.462434465, 1026.874,
     '01010000A0E6100000C1A5C13F351619C0F0DB730D31BB4A40D122DBF97E0B9040'),
    (-6.271684052, 53.462480129, 1021.863,
     '01010000A0E610000033BD1858341619C06C8D828C32BB4A4096438B6CE7EE8F40'),
    (-6.26761737, 53.454251274, 40.245,
     '01010000A0E610000054F7AF490A1219C088FFDEE724BA4A408FC2F5285C1F4440'),
    (98.021272081, 9.36621694, 266.997,
     '01010000A0E6100000A00D93855C815840171569C980BB2240CBA145B6F3AF7040'),
    (98.021745647, 9.366079638, 100.991,
     '01010000A0E610000081ACDA47648158400FE251CA6EBB22408195438B6C3F5940'),
    (98.022163336, 9.366235053, -15.515,
     '01010000A0E610000094D2C41F6B81584052912E2983BB224048E17A14AE072FC0'),
    (98.022560711, 9.366492054, -114.111,
     '01010000A0E6100000D9FA7AA2718158400559B4D8A4BB2240C976BE9F1A875CC0'),
)

@pytest.mark.parametrize('lon, lat, alt, expected', test_positions)
def test_ewkb(lon, lat, alt, expected):
    """
    Test conversion of tuple (longitude, latitude, altitude) to EWKB format.
    """
    result = to_ewkb((lon, lat, alt))
    assert expected == hexlify(result).decode().upper()

# vim: sw=4:et:ai
