# n23-nomad - GPS tracker based on N23 framework
#
# Copyright (C) 2019-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os.path

from setuptools import setup, find_packages

setup(
    name='n23-nomad',
    version='0.1.0',
    description='n23-nomad - GPS tracker based on N23 framework',
    author='Artur Wroblewski',
    author_email='wrobell@riseup.net',
    url='https://gitlab.com/wrobell/n23-nomad',
    setup_requires = ['setuptools_git >= 1.0',],
    packages=find_packages('.'),
    scripts=(
        'bin/n23-nomad',
        'bin/n23-nomad-createdb',
    ),
    include_package_data=True,
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3',
        'Development Status :: 2 - Pre-Alpha',
    ],
    license='GPL',
    install_requires=['n23'],
)

# vim: sw=4:et:ai
