select date_trunc('day', time) as day, min(time), max(time), count(*)
from position
group by day
order by day desc
